// ==UserScript==
// @name         Fuck Boss
// @namespace    http://tampermonkey.net/
// @version      1.1.4
// @description  try to take over the world!
// @author       You
// @match        https://www.zhipin.com/web/boss/recommend
// @downloadURL	 https://gitlab.com/leozhou/fuxxboss-tampermonkey/-/raw/main/fuckboss.js
// @updateURL	 https://gitlab.com/leozhou/fuxxboss-tampermonkey/-/raw/main/fuckboss.js
// @grant        none
// ==/UserScript==

(function () {
	('use strict');

	document.body.insertAdjacentHTML(
		'afterbegin',
		`<button id="hack" style="position:fixed;top:120px;right:15px;z-index:1;font-size:25px;border:0;cursor:pointer;background:transparent">🎃</button>`
	);

	const LOAD_TIME_LIMIT = 50;
	const JAVA_JOB = 'java';
	const WEB_JOB = 'web';
	const TEST_JOB = 'test';
	const ALL_JOB = 'all';
	const TIME_INTERVAL_3_SEC = 3000;
	const TIME_INTERVAL_2_SEC = 2000;

	// LocalStorage key
	const LS_LOAD_TIME_LIMIT_KEY = 'fb_loadTimeLimit';
	const LS_COMPARE_MONTH_MINUEND_KEY = 'fb_compareMonthMinuend';
	const LS_IGNORE_TODAY_COM_KEY = 'fb_ignoreTodayCom';
	const LS_COMMUTE_COUNT_LIMIT_KEY = 'fb_commuteCountLimit';

	// element prefix const
	const DISPLAY_BLOCK_CLASS = 'display-block';
	const ACTIVE_TAG_CLICK_CLASS = 'clicked';
	const ACTIVE_TAG_ID_PRE = 'active_tag';

	const todayZero = new Date(new Date().setHours(0, 0, 0, 0));
	const now = new Date();

	var displayCnt = 0;
	var activeMap = {};
	// 选择的职位，用于过滤不符合经验的候选人
	var chatSelectJob = JAVA_JOB;
	var displayCardList = [];

	// 加载次数
	var loadTimeLimit = LOAD_TIME_LIMIT;
	if (localStorage.getItem(LS_LOAD_TIME_LIMIT_KEY) == undefined) {
		localStorage.setItem(LS_LOAD_TIME_LIMIT_KEY, LOAD_TIME_LIMIT);
	}

	// init compare date
	var compareMonthMinuend = 3;
	if (localStorage.getItem(LS_COMPARE_MONTH_MINUEND_KEY) == undefined) {
		localStorage.setItem(LS_COMPARE_MONTH_MINUEND_KEY, 3);
	}
	var compareDate = now.setMonth(now.getMonth() - compareMonthMinuend);

	// ignore today communicated record
	var ignoreTodayCom = false;
	if (localStorage.getItem(LS_IGNORE_TODAY_COM_KEY) == undefined) {
		localStorage.setItem(LS_IGNORE_TODAY_COM_KEY, false);
	}

	// 沟通次数过滤条件
	var commuteCountLimit = -1;
	if (localStorage.getItem(LS_COMMUTE_COUNT_LIMIT_KEY) == undefined) {
		localStorage.setItem(LS_COMMUTE_COUNT_LIMIT_KEY, -1);
	}

	// add button
	const button = document.getElementById('hack');
	button.addEventListener('click', () => {
		displayCnt = 0;
		displayCardList = [];

		// 重新获取localstorage
		loadTimeLimit = Number(localStorage.getItem(LS_LOAD_TIME_LIMIT_KEY));
		compareMonthMinuend = Number(
			localStorage.getItem(LS_COMPARE_MONTH_MINUEND_KEY)
		);
		ignoreTodayCom =
			localStorage.getItem(LS_IGNORE_TODAY_COM_KEY) === 'true';
		commuteCountLimit = Number(
			localStorage.getItem(LS_COMMUTE_COUNT_LIMIT_KEY)
		);

		compareDate = now.setMonth(now.getMonth() - compareMonthMinuend);

		// 初始化展示的进度
		updateProcessText('0/0/0');

		// 根据选择的职位设置chatSelectJob
		handleTargetJob();

		// 初始化activeTag
		clearActiveTags();

		// get all cards
		const idocument = getIDom();
		const filterCancelBtn = idocument.querySelector('.filter-cancel-btn');
		const filterContainer = idocument.querySelector('.filter-content');
		const handled = filterCancelBtn.style
			? filterCancelBtn.style.display === 'none'
				? false
				: true
			: true;

		if (!handled) {
			filterContainer.style = undefined;

			const novipContainer = idocument.querySelector('.novip');
			const allConditions =
				novipContainer.querySelectorAll('.tipItemList');

			for (var j = 0; j < allConditions.length; j++) {
				const tipItem = allConditions[j];
				const conditions = tipItem.querySelectorAll('a');

				if (j === 0) {
					// experience
					conditions.forEach((element, index) => {
						if (index === 4 || index === 5) {
							element.click();
						}
					});
				} else if (j === 1) {
					// edu
					conditions.forEach((element, index) => {
						if (index > 4) {
							element.click();
						}
					});
				} else if (j === 3) {
					// state
					conditions.forEach((element, index) => {
						if (index === 1 || index > 2) {
							element.click();
						}
					});
				}
			}

			const confirmFilter = idocument.querySelector(
				'.filter-dialog-footer .btn-sure'
			);
			confirmFilter.click();

			filterContainer.style.display = 'none';

			setTimeout(function () {
				loadAllData();
			}, TIME_INTERVAL_3_SEC);
		} else {
			loadAllData();
		}
	});

	function handleTargetJob() {
		chatSelectJob = JAVA_JOB;

		const selectJobEle = getIDom().querySelector(
			'.job-selecter-wrap .ui-dropmenu-label'
		);
		if (selectJobEle.textContent) {
			const jobContent = selectJobEle.textContent;
			if (isAllJob(jobContent)) {
				chatSelectJob = ALL_JOB;
			} else if (isJavaJob(jobContent)) {
				chatSelectJob = JAVA_JOB;
			} else if (isWebJob(jobContent)) {
				chatSelectJob = WEB_JOB;
			} else if (isTestJob(jobContent)) {
				chatSelectJob = TEST_JOB;
			}
		}
	}

	//获取虚拟的document
	function getIDom() {
		return document.querySelector('iframe').contentWindow.document;
	}

	// 执行过滤动作
	function filterAction(idocument) {
		const cards = idocument.querySelectorAll('.geek-info-card');
		console.log(cards);
		loop(cards, 0);
	}

	// 加载所有数据，加载完成后开始执行过滤
	// FIXME 加载数据时，页面未显示，会导致下拉失败
	function loadAllData() {
		var count = 0;
		var loadVal = setInterval(a, TIME_INTERVAL_2_SEC + getRandom() * 1000);
		function a() {
			if (count > loadTimeLimit) {
				console.log('over count');
				clearInterval(loadVal);
				//加载超过50次，先开始过滤
				filterAction(getIDom());
				return;
			}

			const idocument = getIDom();
			const loadMoreEle = idocument.querySelector('.loadmore');
			if (!loadMoreEle) {
				console.log('no loadMore');
				clearInterval(loadVal);
				return;
			}
			const loadMore = loadMoreEle.querySelector('span');
			if (loadMore) {
				count++;
				const text = loadMore.innerText;
				if (!text) {
					console.log('no loadMore text');
					clearInterval(loadVal);
					return;
				}

				if (text.startsWith('没有更多了')) {
					console.log('no more');
					clearInterval(loadVal);

					//加载完成，开始过滤
					filterAction(idocument);

					return;
				} else if (text.startsWith('正在加载数据')) {
					console.log('loading');
				} else {
					// 滚动加载更多
					console.log('load');
					idocument.defaultView.scrollTo(
						0,
						idocument.body.scrollHeight + 100
					);
				}
			} else {
				// <div><div class="no-data-refresh">当前列表没有更多牛人了，刷新获取最新推荐列表</div> <button type="button" class="btn btn-outline btn-refresh">刷新</button></div>
				const noDataRefreshEle =
					loadMoreEle.querySelector('.no-data-refresh');
				if (noDataRefreshEle) {
					console.log('no data refresh');
					clearInterval(loadVal);

					//加载完成，开始过滤
					filterAction(idocument);

					return;
				}
			}
		}
	}

	// card action loop
	function loop(cards, i) {
		if (i > cards.length - 1) {
			console.log('处理完毕');
			if (document.querySelector('.coop-record-wrap .icon-close')) {
				document.querySelector('.coop-record-wrap .icon-close').click();
			}
			console.log(activeMap);
			addActiveTag();
			return;
		}

		// can hide invalid cards for simplify
		/**
		 * if hide all cards, can not continue get next page. leaving more than 6 cards in the page.
		 */
		const couldHideInvalid = displayCnt >= 6 ? true : cards.length - i > 6;

		// get card
		// current index / cards size / display size
		const indexInLog = i + 1 + '/' + cards.length + '/' + displayCnt;
		console.log('处理 cards', indexInLog);
		updateProcessText(indexInLog);
		const card = cards[i];
		const displayBlock = card.querySelector('.' + DISPLAY_BLOCK_CLASS);

		// if has continue button, it means candidate have been greeted
		const continueBtn = card.querySelector('.btn-continue');
		if (continueBtn != null) {
			hideCard(card);

			if (displayBlock) {
				displayBlock.classList.remove(DISPLAY_BLOCK_CLASS);
			}
			// 已沟通，跳过
			// console.log('已沟通过，跳过');
			return loop(cards, i + 1);
		}

		// hide has checked cards, but leave cards needs to display (meeting the conditions)
		if (card.querySelector('.lastTalk') != null) {
			// 已处理过，跳过
			// console.log('已处理过，跳过');
			if (displayBlock == null) {
				// 不需要展示，隐藏
				hideCard(card);
			} else {
				// 需要展示
				addToActiveMap(card);
			}
			return loop(cards, i + 1);
		}

		// get greeting records
		const goutongjilu = card.querySelector('.iboss-goutongjindu-xian');
		if (goutongjilu == null) {
			//未沟通过，校验工作经历
			var expMatched = validWorkExp(card);
			if (expMatched) {
				// add new person text if non-greet
				handleValidCard(card, '🎉发现新人🎉');
			} else {
				// 工作经历未匹配
				if (couldHideInvalid) {
					//可以隐藏无效候选人，则隐藏
					hideCard(card);
				}
				handleInValidCard(card, '工作经历未匹配');
			}
			return loop(cards, i + 1);
		}

		// 模拟点击最近沟通记录按钮
		goutongjilu.click();

		setTimeout(() => {
			var jilu = getCompareJilu(ignoreTodayCom);
			if (!jilu) {
				console.log('未找到沟通记录');
				return;
			}
			const jiluCnt = document.querySelectorAll(
				'.chat-record-content > ul > li'
			).length;
			const msg = jilu.querySelector('.operat').innerText + ':00';

			if (msg) {
				var date = new Date(msg);
				// console.log(msg);
				if (date > compareDate) {
					// 不符合时间要求
					if (
						commuteCountLimit != -1 &&
						jiluCnt < commuteCountLimit
					) {
						//不符合时间要求， 沟通次数小于 commuteCountLimit 次 - 展示
						checkForExp(
							card,
							msg,
							couldHideInvalid,
							'沟通少于' + commuteCountLimit + '次'
						);
					} else {
						//不符合时间要求， 已多人沟通
						handleInValidCard(card, '最后沟通时间：' + msg);
						if (couldHideInvalid) {
							//当前展示数量>=6个，直接隐藏
							hideCard(card);
						}
					}
				} else {
					checkForExp(card, msg, couldHideInvalid);
				}
			} else {
				console.error('未找到沟通记录时间', msg);
				handleInValidCard(card, '最后沟通时间：' + msg);
			}
			loop(cards, i + 1);
		}, 500 + getRandom() * 200);
	}

	function getCompareJilu(ignoreTodayCom) {
		var jilu = document.querySelector('.chat-record-content > ul > li');
		if (!ignoreTodayCom) {
			console.log('不排除今天沟通记录');
			// 不过滤今天沟通记录，直接返回
			return jilu;
		}

		// 第一条沟通时间
		var date = new Date(jilu.querySelector('.operat').innerText + ':00');

		if (todayZero >= date) {
			console.log('第一条沟通时间早于今天0点，不排除');
			return jilu;
		}

		// 第一条沟通时间晚于今天0点
		const jiluList = document.querySelectorAll(
			'.chat-record-content > ul > li'
		);
		for (let j = 0; j < jiluList.length; j++) {
			jilu = jiluList[j];
			date = new Date(jilu.querySelector('.operat').innerText + ':00');
			if (todayZero < date) {
				// 晚于今天0点
				console.log('第' + (j + 1) + '条沟通记录 - 今天沟通过，排除');
			} else {
				break;
			}
		}
		return jilu;
	}

	function checkForExp(card, date, couldHideInvalid, msg) {
		// 符合时间要求，校验工作经历
		var expMatched = validWorkExp(card);
		if (expMatched) {
			handleValidCard(card, msg ? msg : '最后沟通时间：' + date);
		} else {
			// 工作经历未匹配
			if (couldHideInvalid) {
				//可以隐藏无效候选人，则隐藏
				hideCard(card);
			}
			handleInValidCard(card, '工作经历未匹配');
		}
	}

	function handleValidCard(card, msg) {
		card.querySelector('.sider-op').insertAdjacentHTML(
			'afterend',
			`<div class="lastTalk ` +
				DISPLAY_BLOCK_CLASS +
				`" style="position:absolute;top:100px;right:40px;color:red;">` +
				msg +
				`</div>`
		);

		addToActiveMap(card);
	}

	function addToActiveMap(card) {
		pushToActiveMap(card);
		displayCnt = displayCnt + 1;
	}

	function handleInValidCard(card, msg) {
		// card.querySelector('.sider-op').insertAdjacentHTML(
		// 	'afterend',
		// 	`<div class="lastTalk ` +
		// 		DISPLAY_BLOCK_CLASS +
		// 		`" style="position:absolute;top:100px;right:40px">` +
		// 		msg +
		// 		`</div>`
		// );
		card.querySelector('.sider-op').insertAdjacentHTML(
			'afterend',
			`<div class="lastTalk" style="position:absolute;top:100px;right:40px">` +
				msg +
				`</div>`
		);
	}

	function validWorkExp(card) {
		const workExpLiList = card.querySelectorAll(
			'.card-inner .col-3 .work-exp-box li'
		);

		var worExp = '';

		for (var j = 0; j < workExpLiList.length; j++) {
			const workExpLi = workExpLiList[j].querySelectorAll('span');

			for (let k = 0; k < workExpLi.length; k++) {
				const expSpan = workExpLi[k];
				if (expSpan) {
					worExp += expSpan.textContent.trim() + ',';
				}
			}

			worExp += ';';
		}

		if (
			isJavaJob(worExp) &&
			(chatSelectJob === JAVA_JOB || chatSelectJob === ALL_JOB)
		) {
			return true;
		} else if (
			isWebJob(worExp) &&
			(chatSelectJob === WEB_JOB || chatSelectJob === ALL_JOB)
		) {
			return true;
		} else if (
			isTestJob(worExp) &&
			(chatSelectJob === TEST_JOB || chatSelectJob === ALL_JOB)
		) {
			return true;
		} else {
			console.log(worExp);
			return false;
		}
	}

	// init get random number method for sleeping
	function getRandom() {
		return Math.random();
	}

	function upperIncludes(str1, str2) {
		return str1.toUpperCase().includes(str2.toUpperCase());
	}

	function isJavaJob(jobContent) {
		return upperIncludes(jobContent, 'java');
	}

	function isWebJob(jobContent) {
		return (
			upperIncludes(jobContent, 'web') ||
			upperIncludes(jobContent, '前端') ||
			upperIncludes(jobContent, 'html') ||
			upperIncludes(jobContent, 'javascript') ||
			upperIncludes(jobContent, 'css') ||
			upperIncludes(jobContent, 'h5')
		);
	}
	function isTestJob(jobContent) {
		return upperIncludes(jobContent, '测试');
	}
	function isAllJob(jobContent) {
		return upperIncludes(jobContent, '全部');
	}

	function pushToActiveMap(card) {
		const activeEle = card.querySelector('.card-inner .col-2 .name span');
		if (activeEle) {
			const text = activeEle.innerText;
			if (!activeMap[text]) {
				activeMap[text] = [];
			}
			activeMap[text].push(card);
		}
	}

	function addActiveTag() {
		var tagsHtml = '';

		const keys = Object.keys(activeMap).sort();
		keys.forEach((key, index) => {
			const value = activeMap[key];
			tagsHtml +=
				'<button id="' +
				ACTIVE_TAG_ID_PRE +
				index +
				'" data="' +
				key +
				'" class="active-tag-btn" style="top:' +
				(160 + index * 35) +
				'px;">' +
				key +
				'/' +
				(value ? value.length : 0) +
				'</button>';
		});

		document.body.insertAdjacentHTML('afterbegin', tagsHtml);

		const tagsCss = `<style type="text/css">

        .active-tag-btn {
            position:fixed;
            right:15px;
            z-index:1;
            font-size: 15px;
            background-color: #ccc;
            border:0;
            cursor:pointer;
            color: #666;
            padding: 0px 6px;
        }

        .active-tag-btn.clicked{
            background-color: #eee;
            border-left: solid #8EE1D8;
        }

        </style>`;
		document.body.insertAdjacentHTML('afterbegin', tagsCss);

		keys.forEach((key, index) => {
			const tagBtn = document.getElementById(ACTIVE_TAG_ID_PRE + index);
			if (!tagBtn) {
				return;
			}
			tagBtn.addEventListener('click', () => {
				const classList = tagBtn.classList;
				const clicked = classList.contains(ACTIVE_TAG_CLICK_CLASS);
				clearTagBtnClicked();

				hideAllCards();

				if (!clicked) {
					classList.add(ACTIVE_TAG_CLICK_CLASS);

					displayByTag(key);
				} else {
					displayByTag();
				}
			});
		});
		for (const [key, value] of Object.entries(activeMap)) {
		}
	}

	function hideCard(card) {
		if (card.style.display === 'none') {
			return;
		}
		card.style.display = 'none';
	}

	function displayCard(card) {
		if (card.style && card.querySelector('.' + DISPLAY_BLOCK_CLASS)) {
			card.style = undefined;
		}
	}

	function hideAllCards() {
		const cards = getIDom().querySelectorAll('.geek-info-card');
		cards.forEach((card, index) => {
			hideCard(card);
		});
	}

	function displayByTag(key) {
		if (key) {
			[...activeMap[key]].forEach((card, index) => {
				displayCard(card);
			});
		} else {
			for (const [key, values] of Object.entries(activeMap)) {
				[...values].forEach((card, index) => {
					displayCard(card);
				});
			}
		}
	}

	function clearTagBtnClicked() {
		for (var j = 0; j < Object.entries(activeMap).length; j++) {
			const tagBtn = document.getElementById(ACTIVE_TAG_ID_PRE + j);
			tagBtn.classList.remove(ACTIVE_TAG_CLICK_CLASS);
		}
	}

	function isEmptyObj(obj) {
		for (var key in obj) {
			return false;
		}
		return true;
	}

	function clearActiveTags() {
		if (isEmptyObj(activeMap)) {
			return;
		}

		displayByTag();

		const tagBtnEleList = document.querySelectorAll('.active-tag-btn');
		if (tagBtnEleList) {
			tagBtnEleList.forEach((tagBtn) => {
				if (tagBtn) {
					tagBtn.parentNode.removeChild(tagBtn);
				}
			});
		}

		activeMap = {};
	}

	function updateProcessText(process) {
		const pageHeader = document.querySelector('.page-header');
		if (!pageHeader) {
			return;
		}

		const processEle = pageHeader.querySelector('.fb-process');
		const processText = '过滤进度：' + process;
		if (processEle) {
			processEle.innerText = processText;
		} else {
			pageHeader.insertAdjacentHTML(
				'beforeend',
				`<div class="fb-process page-name" >` + processText + `</div>`
			);
		}
	}
})();
